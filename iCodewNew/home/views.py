from django.shortcuts import render, HttpResponse, redirect
from home.models import Contact
from django.contrib import messages
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from blog.models import Post

# Create your views here.
#HTML coding
def home(request):
    return render(request, 'home/home.html')

def about(request):
    messages.success(request, 'This is about')
    return render(request, 'home/about.html')

def contact(request):
    messages.success(request, 'Welcome to contact')
    if request.method=="POST":
        print("we are using post request")
        name = request.POST['name']
        email = request.POST['email']
        phone = request.POST['phone']
        content = request.POST['content']

        contact = Contact(name = name, email=email, phone = phone, content=content)
        if len(name) <2 or len(email)<3 or len(phone)<10 or len(content)<4:
            messages.error(request, 'Kindly check the details ')
        else:
            messages.success(request, 'Message sent successfully')
        contact.save()
    
    return render(request, 'home/contact.html')


def search(request):
    query = request.GET['query']
    if len(query) > 78:
        allPosts = Post.objects.none()
    else:
        allPostsTitle = Post.objects.filter(title__icontains = query)
        allPostsContent = Post.objects.filter(content__icontains = query)
        allPosts = allPostsTitle.union(allPostsContent)

        if allPosts.count() == 0:
            messages.warning(request, 'No search results found please redefine your query ')
    params = {'allPosts':allPosts, 'query': query}
    return render(request,'home/search.html', params)


# Authentication API
def handelSignup(request):
    if request.method =='POST':
        # get the post parameters
        username = request.POST['username']
        firstname = request.POST['firstname']
        lastname = request.POST['lastname']
        email = request.POST['email']
        pass1 = request.POST['pass1']
        pass2 = request.POST['pass2']

        # username should be under 15 characters
        if len(username)>15:
            messages.error(request, "Username shoud be less than 15 characters")
            return redirect('home')

        # username should be alphanumeric
        if not username.isalnum():
            messages.error(request, "Username must be alphanumeric")
            return redirect('home')

        # password should not missmatch
        if pass1 != pass2:
            messages.error(request, "Passwords do not match")
            return redirect('home')



        #checks for erronious inputs
        #Create the user 

        myuser = User.objects.create_user(username, email, pass1)
        myuser.first_name = firstname
        myuser.last_name = lastname
        myuser.save()
        messages.success(request, "Your ICoder account is created successfully!!")
        return redirect('home')
    else:
        return HttpResponse('404 - Not found')


def handelLogin(request):
    if request.method =='POST':
        # get the post parameters
        loginusername = request.POST['loginusername']
        loginpassword = request.POST['loginpassword']
        
        user = authenticate(username=loginusername, password=loginpassword)

        if user is not None:
            login(request, user)
            messages.success(request, 'Login Success')
            return redirect('home')
            
        else:
            messages.error(request, 'Invalid Credentials, pleas try again')
            return redirect('home')
       
    return HttpResponse('404 - Not found')

def handelLogout(request):

    logout(request)
    messages.success(request, 'Logout Success!!')
    return redirect('home')
